import "./App.css";
import React from "react";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      showH1: true,
      showDiv: true,
      showH2: true,
      showH3: true,
      showB: true,
    };
  }

  hideButton = () => {
    this.setState({ show: false });
  };

  showButton = () => {
    this.setState({ show: true });
  };

  hideH1Button = () => {
    this.setState({ showH1: false });
  };

  showH1Button = () => {
    this.setState({ showH1: true });
  };

  hideDivButton = () => {
    this.setState({ showDiv: false });
  };

  showDivButton = () => {
    this.setState({ showDiv: true });
  };

  hideH2Button = () => {
    this.setState({ showH2: false });
  };

  showH2Button = () => {
    this.setState({ showH2: true });
  };

  showH2Button = () => {
    this.setState({ showH2: true });
  };

  buttonH3 = () => {
    const { showH3 } = this.state;
    this.setState({ showH3: !showH3 });
  };

  buttonB = () => {
    const { showB } = this.state;
    this.setState({ showB: !showB });
  };

  render() {
    return (
      <>
        <div>
          <button onClick={this.hideButton}>Esconda a p!</button>
          <button onClick={this.showButton}>Aparecer a p!</button>
          {this.state.show ? <p> Paragrafo</p> : null}
        </div>
        <div>
          <button onClick={this.hideH1Button}>Esconda a h1</button>
          <button onClick={this.showH1Button}>Aparecer a h1</button>
          {this.state.showH1 ? <h1> h1</h1> : null}
        </div>
        <div>
          <button onClick={this.hideDivButton}>Esconda a Div!</button>
          <button onClick={this.showDivButton}>Aparecer a Div!</button>
          {this.state.showDiv ? <div> Div</div> : null}
        </div>
        <div>
          <button onClick={this.hideH2Button}>Esconda a h2</button>
          <button onClick={this.showH1Button}>Aparecer a h2</button>
          {this.state.showH2 ? <h2> h2!</h2> : null}
        </div>
        <div>
          <button onClick={this.buttonH3}>
            {this.state.showH3 ? "Esconde" : "Mostra"}
          </button>
          {this.state.showH3 ? <h3> h3!</h3> : null}
        </div>
        <div>
          <button onClick={this.buttonB}>
            {this.state.showB ? "Esconde" : "Mostra"}
          </button>
          <div>{this.state.showB ? <b> B</b> : null}</div>
        </div>
      </>
    );
  }
}

export default App;
